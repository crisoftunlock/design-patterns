import { useEffect, useState } from "react"
import CreateRequest from "./patterns/factory";

function App() {

  const [products, setProducts] = useState([]);

  useEffect(() => {
    (async () => {
      const data = await CreateRequest.request('/');
      setProducts(data);
    })()
  },[]);

  return (
    <>
      {products.map((item) => <p key={item.id}>{item.title}</p>)}
    </>
  )
}

export default App
