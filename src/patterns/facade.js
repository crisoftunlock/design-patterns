// Módulo de inventario
class Inventory {
    checkAvailability(item) {
      // Lógica para verificar la disponibilidad del artículo en el inventario
      console.log(`Verificando disponibilidad de ${item} en el inventario...`);
      // Devuelve true o false según la disponibilidad
      return true;
    }
  }
  
  // Módulo de carrito de compras
  class ShoppingCart {
    addToCart(item) {
      // Lógica para agregar un artículo al carrito de compras
      console.log(`Agregando ${item} al carrito de compras...`);
    }
  }
  
  // Módulo de pago
  class Payment {
    processPayment(amount) {
      // Lógica para procesar el pago
      console.log(`Procesando el pago de ${amount}...`);
    }
  }
  
  // Fachada
  class OnlineStore {
    constructor() {
      this.inventory = new Inventory();
      this.shoppingCart = new ShoppingCart();
      this.payment = new Payment();
    }
  
    purchaseItem(item, amount) {
      if (this.inventory.checkAvailability(item)) {
        this.shoppingCart.addToCart(item);
        this.payment.processPayment(amount);
        console.log(`Compra de ${item} exitosa.`);
      } else {
        console.log(`El artículo ${item} no está disponible en el inventario.`);
      }
    }
  }
  
  // Uso de la fachada
  export const storeFacade = new OnlineStore();
  storeFacade.purchaseItem("Camiseta", 29.99);