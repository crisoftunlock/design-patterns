// Definimos la clase Subject
class Subject {
    constructor() {
      this.observers = []; // Array para almacenar los observadores
    }
  
    // Método para registrar un observador
    addObserver(observer) {
      this.observers.push(observer);
    }
  
    // Método para eliminar un observador
    removeObserver(observer) {
      const index = this.observers.indexOf(observer);
      if (index !== -1) {
        this.observers.splice(index, 1);
      }
    }
  
    // Método para notificar a todos los observadores
    notifyObservers() {
      this.observers.forEach(observer => {
        observer.update();
      });
    }
  }
  
  // Definimos la clase Observer
  class Observer {
    constructor(name) {
      this.name = name;
    }
  
    // Método para actualizar el observador
    update() {
      console.log(`El observador ${this.name} ha sido notificado.`);
    }
  }
  
  // Creamos una instancia del sujeto
  const subject = new Subject();
  
  // Creamos instancias de observadores
  const observer1 = new Observer("Observador 1");
  const observer2 = new Observer("Observador 2");
  
  // Registramos los observadores en el sujeto
  subject.addObserver(observer1);
  subject.addObserver(observer2);
  
  // Notificamos a los observadores
  subject.notifyObservers();
  
  // Salida esperada:
  // El observador Observador 1 ha sido notificado.
  // El observador Observador 2 ha sido notificado.