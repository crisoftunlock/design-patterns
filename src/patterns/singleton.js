class DatabaseConnection {
  constructor() {
    // Aquí se realizaría la lógica de conexión a la base de datos
    console.log('Conexión a la base de datos establecida');
  }

  query(sql) {
    // Aquí se realizaría la lógica de ejecución de consultas SQL
    console.log(`Ejecutando consulta: ${sql}`);
  }
}

class DatabaseSingleton {
  constructor() {
    if (!DatabaseSingleton.instance) {
      DatabaseSingleton.instance = new DatabaseConnection();
    }
  }

  getInstance() {
    return DatabaseSingleton.instance;
  }
}

// Uso del Singleton para obtener la instancia de la conexión a la base de datos
const dbInstance1 = new DatabaseSingleton().getInstance();
const dbInstance2 = new DatabaseSingleton().getInstance();

// Ambas instancias son la misma
console.log(dbInstance1 === dbInstance2); // true

// Ejecución de una consulta en la base de datos
dbInstance1.query('?=');