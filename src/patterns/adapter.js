// Interfaz común para mostrar y manipular mapas

import { GoogleMaps } from "../gooleMaps";


class MapInterface {
    constructor() {
      // ...
    }
  
    showMap() {
      // ...
    }
  
    zoomIn() {
      // ...
    }
  
    zoomOut() {
      // ...
    }
  }
  
  // Adaptador para Google Maps
  class GoogleMapsAdapter extends MapInterface {
    constructor(googleMap) {
      super();
      this.googleMap = googleMap;
    }
  
    showMap() {
      this.googleMap.display();
    }
  
    zoomIn() {
      this.googleMap.zoomIn();
    }
  
    zoomOut() {
      this.googleMap.zoomOut();
    }
  }
  
  
  // Uso de los adaptadores
  const googleMap = new GoogleMaps(); // Supongamos que GoogleMap es una clase proporcionada por la API de Google Maps
  
  const googleMapsAdapter = new GoogleMapsAdapter(googleMap);
  
  googleMapsAdapter.showMap(); // Muestra el mapa utilizando Google Maps
  googleMapsAdapter.zoomIn(); // Hace zoom en el mapa utilizando Google Maps
