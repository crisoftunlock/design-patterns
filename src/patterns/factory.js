class Adapter {
    constructor(url, body) {
        this.url = 'https://fakestoreapi.com/products' + url;
        this.body = body;
    }
}

class Get extends Adapter {
    constructor(url, body) {
        super(url, body);
        this.method = 'GET';
    }
    response = () => fetch(this.url).then(res => res.json());
}

class Post extends Adapter {
    constructor(url, body) {
        super(url, body);
        this.method = 'POST';
    }
    response = () => fetch(this.url, {
        method: this.method,
        body: JSON.stringify(this.body)
    }).then(res => res.json())
}

class Put extends Adapter {
    constructor(url, body) {
        super(url, body);
        this.method = 'PUT';
    }
    response = () => fetch(this.url, {
        method: this.method,
        body: JSON.stringify(this.body)
    }).then(res => res.json())
}

class Patch extends Adapter {
    constructor(url, body) {
        super(url, body);
        this.method = 'PATCH'
    }
    response = () => fetch(this.url, {
        method: this.method,
        body: JSON.stringify(this.body)
    }).then(res => res.json())
}

class CreateRequest {
    static request(url, method, body) {
        switch (method) {
            case 'GET':
                return new Get(url, body).response();

            case 'POST':
                return new Post(url, body).response();

            case 'PUT':
                return new Put(url, body).response();

            case 'PATCH':
                return new Patch(url, body).response();

            default:
                return new Get(url, body).response();
        }
    }
}

export default CreateRequest;